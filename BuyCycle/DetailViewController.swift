//
//  DetailViewController.swift
//  BuyCycle
//
//  Created by babycougar on 15.04.2021.
//  Copyright © 2021 individualProject. All rights reserved.
//
import UIKit



private enum LayoutValues {
    static let imageSize: CGFloat = 240
    static let padding: CGFloat = 8
    static let padding2x: CGFloat = 16
}

class DetailViewController: UIViewController {
        
    // MARK: - Private properties
    
    
    private var bike: Bike
    
    private var bikeImageView: UIImageView!
    private var titleLabel: UILabel!
    private var sizeLabel: UILabel!
    private var codLabel: UILabel!
    private var priceLabel: UILabel!
    private var descriptionLabel: UILabel!
    
    private var backButton: UIButton!
    
    // MARK: - Init
    
    init(bike: Bike) {
        self.bike = bike
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        initConstraints()
    }
    
    // MARK: - UI
    
    private func initUI() {
        edgesForExtendedLayout = []
        view.backgroundColor = .darkColor
        navigationItem.title = "Details"
        
        bikeImageView = UIImageView()
        bikeImageView.contentMode = .scaleAspectFit
        bikeImageView.image = UIImage(named: bike.imageName)
        view.addSubview(bikeImageView)
        
        titleLabel = UILabel()
        titleLabel.textAlignment = .center
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        titleLabel.text = bike.title
        view.addSubview(titleLabel)
        
        sizeLabel = UILabel()
        sizeLabel.textAlignment = .center
        sizeLabel.textColor = .white
        sizeLabel.font = UIFont.systemFont(ofSize: 14)
        sizeLabel.text = bike.size
        view.addSubview(sizeLabel)
        
        codLabel = UILabel()
        codLabel.textAlignment = .center
        codLabel.textColor = .gray
        codLabel.font = UIFont.systemFont(ofSize: 10)
        codLabel.text = bike.cod
        view.addSubview(codLabel)
        
        priceLabel = UILabel()
        priceLabel.textAlignment = .center
        priceLabel.textColor = .sysBlue
        priceLabel.font = UIFont.systemFont(ofSize: 20)
        priceLabel.text = bike.price
        view.addSubview(priceLabel)

        
        descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .left
        descriptionLabel.textColor = .white
        descriptionLabel.font = UIFont.systemFont(ofSize: 14)
        descriptionLabel.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        view.addSubview(descriptionLabel)
        
        backButton = UIButton()
        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(.white, for: .normal)
        backButton.addTarget(self, action: #selector(didTapBackButton), for: .touchUpInside)
        view.addSubview(backButton)
    }
    
    private func initConstraints() {
        bikeImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        sizeLabel.translatesAutoresizingMaskIntoConstraints = false
        codLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        backButton.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            backButton.topAnchor.constraint(equalTo: view.topAnchor, constant: .padding2x),
            backButton.leadingAnchor.constraint(equalTo: view.leadingAnchor),

            bikeImageView.topAnchor.constraint(equalTo: backButton.bottomAnchor, constant: LayoutValues.padding2x),
            bikeImageView.widthAnchor.constraint(equalToConstant: LayoutValues.imageSize),
            bikeImageView.heightAnchor.constraint(equalToConstant: LayoutValues.imageSize),
            bikeImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: bikeImageView.bottomAnchor, constant: LayoutValues.padding2x),
            titleLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: LayoutValues.padding2x),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -LayoutValues.padding2x),
            
            sizeLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: LayoutValues.padding),
            sizeLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: LayoutValues.padding2x),
            sizeLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -LayoutValues.padding2x),
            
            codLabel.topAnchor.constraint(equalTo: sizeLabel.bottomAnchor, constant: LayoutValues.padding),
            codLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: LayoutValues.padding2x),
            codLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -LayoutValues.padding2x),
            
            priceLabel.topAnchor.constraint(equalTo: codLabel.bottomAnchor, constant: LayoutValues.padding),
            priceLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: LayoutValues.padding2x),
            priceLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -LayoutValues.padding2x),
            
            descriptionLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: LayoutValues.padding2x),
            descriptionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: LayoutValues.padding2x),
            descriptionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -LayoutValues.padding2x)
        ])
    }
    
    @objc private func didTapBackButton() {
        let storeViewcontroller = StoreViewController()
        //navigationController?.pushViewController(detailViewController, animated: true)
        self.present(storeViewcontroller, animated: true, completion: nil)
    }

}


    

