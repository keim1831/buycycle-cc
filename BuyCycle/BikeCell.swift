//
//  BikeCell.swift
//  BuyCycle
//
//  Created by babycougar on 15.04.2021.
//  Copyright © 2021 individualProject. All rights reserved.
//
import UIKit
/*
protocol BikeCellDelegate: AnyObject {
    func didTapOpenButton(bike: Bike)
}
 */

private enum LayoutValues {
    static let thumbnailSize: CGFloat = 48
    static let smallPadding: CGFloat = 4
    static let padding: CGFloat = 8
    static let padding2x: CGFloat = 16
    static let buttonWidth: CGFloat = 60
    static let buttonHeight: CGFloat = 40
}

class BikeCell: UITableViewCell {
    
    // MARK: - Public properties
    
    //weak var delegate: BikeCellDelegate?
    
    var bike: Bike? {
        didSet {
            updateUI()
        }
    }
    
    // MARK: - Private properties
    
    private var thumbnailImageView: UIImageView!
    
    private var contentStackView: UIStackView!
    private var titleLabel: UILabel!
    private var sizeLabel: UILabel!
    private var codLabel: UILabel!
    private var priceLabel: UILabel!
    //private var openButton: UIButton!
    private var path: UIImageView!
    
    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initUI()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    
    private func initUI() {
        
        thumbnailImageView = UIImageView()
        thumbnailImageView.contentMode = .scaleAspectFill
        thumbnailImageView.layer.cornerRadius = 8
        thumbnailImageView.layer.masksToBounds = true
        contentView.addSubview(thumbnailImageView)
        
        contentStackView = UIStackView()
        //contentStackView.backgroundColor = .darkColor
        contentStackView.axis = .vertical
        contentView.addSubview(contentStackView)
        
        titleLabel = UILabel()
        titleLabel.textColor = .white
        titleLabel.numberOfLines = 1
        titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        contentStackView.addArrangedSubview(titleLabel)
        
        sizeLabel = UILabel()
        sizeLabel.textColor = .white
        sizeLabel.numberOfLines = 1
        sizeLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        contentStackView.addArrangedSubview(sizeLabel)

        codLabel = UILabel()
        codLabel.textColor = .gray
        codLabel.numberOfLines = 1
        codLabel.font = UIFont.systemFont(ofSize: 10, weight: .semibold)
        contentStackView.addArrangedSubview(codLabel)
        
        
        priceLabel = UILabel()
        priceLabel.textColor = .sysBlue
        priceLabel.numberOfLines = 1
        priceLabel.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        contentStackView.addArrangedSubview(priceLabel)
        
        path = UIImageView()
        path.image = .path
        path.contentMode = .scaleAspectFit
        contentView.addSubview(path)
        /*
        openButton = UIButton()
        //openButton.backgroundColor = .darkColor
        //openButton.layer.cornerRadius = 10
        openButton.layer.masksToBounds = true
        //openButton.titleLabel?.font = .systemFont(ofSize: 20)
        openButton.setImage(.path, for: .normal)
        openButton.addTarget(self, action: #selector(didTapOpenButton), for: .touchUpInside)
        contentView.addSubview(openButton)
        */
    }
    
    private func initConstraints() {
        thumbnailImageView.translatesAutoresizingMaskIntoConstraints = false
        contentStackView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        sizeLabel.translatesAutoresizingMaskIntoConstraints = false
        codLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        path.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            thumbnailImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: LayoutValues.padding2x),
            thumbnailImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            contentStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            contentStackView.leadingAnchor.constraint(equalTo: thumbnailImageView.trailingAnchor, constant: LayoutValues.padding),
            contentStackView.trailingAnchor.constraint(lessThanOrEqualTo: path.leadingAnchor, constant: -LayoutValues.padding),
            
            path.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            path.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -LayoutValues.padding2x)
            
        ])
    }

    private func updateUI() {
        guard let bike = bike else { return }
        thumbnailImageView.image = UIImage(named: bike.imageName)
        titleLabel.text = bike.title
        sizeLabel.text = bike.size
        codLabel.text = bike.cod
        priceLabel.text = bike.price
    }
    
    /*
    // MARK: - Actions
    
    @objc private func didTapOpenButton() {
        guard let bike = bike else { return }
        delegate?.didTapOpenButton(bike: bike)
    }*/
}
