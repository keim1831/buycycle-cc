//
//  ImageTitleView.swift
//  BuyCycle
//
//  Created by babycougar on 15/04/2021.
//  Copyright © 2021 individualProject. All rights reserved.
//

import UIKit

final class ImageTitleView: UIView {
    
    // MARK: - Public properties
    
    var name: String? {
        didSet {
            button.setTitle(name, for: .normal)
        }
    }
    
    var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    
    // MARK: - Private properties
    
    private var imageView: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    private var button: UIButton = {
        let button = UIButton()
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)
        button.titleLabel?.textAlignment = .left
        //button.contentHorizontalAlignment = .left
        return button
    }()
    
    private var containerStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 4.0
        stackView.distribution = .fill
        stackView.alignment = .center
        return stackView
    }()
    
    // MARK: - UI
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initUI()
        initLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initUI() {
        backgroundColor = .darkColor
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(containerStackView)
        containerStackView.addArrangedSubview(imageView)
        containerStackView.addArrangedSubview(button)
    }
    
    private func initLayout() {
        NSLayoutConstraint.activate([
            containerStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerStackView.topAnchor.constraint(equalTo: topAnchor),
            containerStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            //button.leftAnchor.constraint(equalTo: imageView.leftAnchor)
            button.widthAnchor.constraint(equalTo: imageView.widthAnchor)
        ])
    }
}
