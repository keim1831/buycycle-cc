//
//  ViewController.swift
//  BuyCycle
//
//  Created by babycougar on 15/04/2021.
//  Copyright © 2021 individualProject. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - Properties
    
    private var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = .bgImage
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private var emailInput: UITextField = {
        let input = UITextField()
        input.font = UIFont.systemFont(ofSize: 16.0)
        input.textColor = .white
        input.attributedPlaceholder = NSAttributedString(string: "Enter e-mail", attributes:[NSAttributedString.Key.foregroundColor:UIColor.white])
        input.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: input.frame.height))
        input.leftViewMode = .always
        input.backgroundColor = .lightGray
        input.layer.cornerRadius = 8.0
        return input
    }()
    
    
    private var passwordInput: UITextField = {
        let input = UITextField()
        input.isSecureTextEntry = true
        input.font = UIFont.systemFont(ofSize: 16.0)
        input.textColor = .white
        input.attributedPlaceholder = NSAttributedString(string: "Enter password", attributes:[NSAttributedString.Key.foregroundColor:UIColor.white])
        input.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: input.frame.height))
        input.leftViewMode = .always
        input.backgroundColor = .lightGray
        input.layer.cornerRadius = 8.0
        return input
    }()
    
    private var confirmPasswordInput: UITextField = {
        let input = UITextField()
        input.isSecureTextEntry = true
        input.font = UIFont.systemFont(ofSize: 16.0)
        input.textColor = .white
        input.attributedPlaceholder = NSAttributedString(string: "Enter confirm password", attributes:[NSAttributedString.Key.foregroundColor:UIColor.white])
        input.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: input.frame.height))
        input.leftViewMode = .always
        input.backgroundColor = .lightGray
        input.layer.cornerRadius = 8.0
        return input
    }()
    
    private var signUpLabel: UILabel = {
        let label = UILabel()
        label.text = "Sign Up"
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 30.0)
        label.alpha = 1.0
        return label
    }()
    
    private var signUpButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .sysBlue
        button.setTitle("Sign Up", for: .normal)
        button.addTarget(self, action: #selector(didTapSignUp), for: .touchUpInside)
        button.setTitleColor(.white, for: .normal)
        button.contentHorizontalAlignment = .center
        button.layer.cornerRadius = 8.0
        return button
    }()
    
    private var question: UILabel = {
        let label = UILabel()
        label.text = "Already have account?"
        label.textColor = .gray
        label.font = UIFont.boldSystemFont(ofSize: 18.0)
        label.alpha = 1.0
        return label
    }()
    
    private var signInButton: UIButton = {
        var button = UIButton()
        button.setTitle("Sign In", for: .normal)
        button.setTitleColor(.sysBlue, for: .normal)
        button.addTarget(self, action: #selector(didTapSignIn), for: .touchUpInside)
        //button.alpha = 0.0
        
        return button
    }()
    
    
    // MARK: - UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        initLayout()        
    }
    
    private func initUI() {
        
        view.backgroundColor = .darkColor
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        emailInput.translatesAutoresizingMaskIntoConstraints = false
        passwordInput.translatesAutoresizingMaskIntoConstraints = false
        confirmPasswordInput.translatesAutoresizingMaskIntoConstraints = false
        signUpLabel.translatesAutoresizingMaskIntoConstraints = false
        signUpButton.translatesAutoresizingMaskIntoConstraints = false
        question.translatesAutoresizingMaskIntoConstraints = false
        signInButton.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(imageView)
        view.addSubview(emailInput)
        view.addSubview(signUpLabel)
        view.addSubview(passwordInput)
        view.addSubview(confirmPasswordInput)
        view.addSubview(signUpButton)
        view.addSubview(question)
        view.addSubview(signInButton)
        
    }
    
    private func initLayout() {
        

        
        let constraints : [NSLayoutConstraint] = [
            
            imageView.topAnchor.constraint(equalTo: view.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            //imageView.heightAnchor.constraint(lessThanOrEqualToConstant: view.frame.height/2),
            //imageView.widthAnchor.constraint(lessThanOrEqualToConstant: view.frame.height/2),
            //imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            //imageView.centerYAnchor.constraint(lessThanOrEqualTo: view.centerYAnchor),
            
            emailInput.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: .padding2x),
            emailInput.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding4x),
            emailInput.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: (-.padding4x)),
            emailInput.heightAnchor.constraint(equalToConstant: view.frame.height / 17),
            emailInput.widthAnchor.constraint(equalToConstant: view.frame.width - .padding2x),
            
            signUpLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -.padding4x * 2),
            signUpLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            passwordInput.topAnchor.constraint(equalTo: emailInput.bottomAnchor, constant: .padding2x),
            passwordInput.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding4x),
            passwordInput.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.padding4x),
            passwordInput.heightAnchor.constraint(equalToConstant: view.frame.height / 17),
            passwordInput.widthAnchor.constraint(equalToConstant: view.frame.width - .padding2x),
            
            confirmPasswordInput.topAnchor.constraint(equalTo: passwordInput.bottomAnchor, constant: .padding2x),
            confirmPasswordInput.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding4x),
            confirmPasswordInput.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: (-.padding4x)),
            confirmPasswordInput.heightAnchor.constraint(equalToConstant: view.frame.height / 17),
            confirmPasswordInput.widthAnchor.constraint(equalToConstant: view.frame.width - .padding2x),
            
            signUpButton.heightAnchor.constraint(equalToConstant: view.frame.height / 17),
            signUpButton.widthAnchor.constraint(equalToConstant: view.frame.width - .padding2x),
            signUpButton.topAnchor.constraint(lessThanOrEqualTo: confirmPasswordInput.bottomAnchor, constant: .padding4x * 2),
            signUpButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding4x),
            signUpButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: (-.padding4x)),
            
            //question.topAnchor.constraint(equalTo: signUpButton.bottomAnchor, constant: .padding),
            question.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -.padding4x),
            question.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            //signInButton.centerXAnchor.constraint(greaterThanOrEqualTo: view.centerXAnchor),
            signInButton.leadingAnchor.constraint(equalTo: question.trailingAnchor, constant: .padding),
            signInButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -.padding4x + 5)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc private func didTapSignUp() {
        let controller = StoreViewController()
        //let navigation = UINavigationController(rootViewController: controller)
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc private func didTapSignIn() {
        let controller = SignInViewController()
        //let navigation = UINavigationController(rootViewController: controller)
        self.present(controller, animated: true, completion: nil)
    }
    

}

