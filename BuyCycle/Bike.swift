//
//  Bike.swift
//  BuyCycle
//
//  Created by babycougar on 15.04.2021.
//  Copyright © 2021 individualProject. All rights reserved.
//

import Foundation

struct Bike {
    let imageName: String
    let title: String
    let size: String
    let cod: String
    let price: String
}

extension Bike {
    
    static let availableBikes = [
        Bike(imageName: "centurion", title: "CENTURION BACKFIRE", size: "600 - 26''", cod: "Cod: CBF600261301B", price: "3.667 lei"),
        Bike(imageName: "whirlwind", title: "NEWUZER  WHIRLWIND", size: "50 - 28''", cod: "Cod: CBF600261301B", price: "2.179 lei"),
        Bike(imageName: "viper", title: "KELLYS VIPER 50", size: "27.5''", cod: "Cod: CBF600261301B", price: "2.110 lei"),
        Bike(imageName: "jumbo", title: "NEUZER JUMBO SPORT", size: "H - 29''", cod: "Cod: CBF600261301B", price: "2.319 lei")
        
    ]
}
