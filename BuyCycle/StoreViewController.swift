//
//  StoreViewController.swift
//  BuyCycle
//
//  Created by babycougar on 15/04/2021.
//  Copyright © 2021 individualProject. All rights reserved.
//

import UIKit

class StoreViewController: UIViewController {
    
    // MARK: - Properties
    
    private var tableView: UITableView!
    
    private var searchInput: UITextField = {
        let input = UITextField()
        input.font = UIFont.systemFont(ofSize: 25.0)
        input.textColor = .white
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = .search
        let attachmentString = NSAttributedString(attachment:  imageAttachment)
        let completeText = NSMutableAttributedString(string: "  ")
        completeText.append(attachmentString)
        //let textAfterIcon = NSAttributedString(string: " Search product")
        
        
        let textAfterIcon = NSAttributedString(string: " Search product", attributes:[NSAttributedString.Key.font: UIFont.systemFont(ofSize: 25.0), NSAttributedString.Key.foregroundColor:UIColor.white])
                
        completeText.append(textAfterIcon)
  

        input.attributedPlaceholder = completeText
        //input.attributedPlaceholder = NSAttributedString(string: "Search product", attributes:[NSAttributedString.Key.foregroundColor:UIColor.white])
        input.leftView = UIView(frame: CGRect(x: 10, y: 10, width: 10, height: input.frame.height))
        input.leftViewMode = .always
        input.backgroundColor = .lightGray
        input.layer.cornerRadius = 10
        return input
    }()
    
    private var cityBikesCategory: ImageTitleView = {
        let view = ImageTitleView()
        view.image = .cityBikes
        view.name = "City Bikes"
        return view
    }()
    
    private var mountainBikesCategory: ImageTitleView = {
        let view = ImageTitleView()
        view.image = .mountainBikes
        view.name = "Mountain Bikes"
        return view
    }()
    
    private var categoryStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .top
        //stackView.distribution = .equalSpacing
        return stackView
    }()
    
    
    private var forYouLabel: UILabel = {
        let label = UILabel()
        label.text = "Bikes for you"
        label.font = UIFont.boldSystemFont(ofSize: 25.0)
        label.textColor = .white
        return label
    }()
    
    // MARK: - UI
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        initLayout()
    }
    
    private func initUI() {
        
        tableView = UITableView()
        tableView.backgroundColor = .darkColor
        tableView.rowHeight = 120
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(BikeCell.self, forCellReuseIdentifier: "bikeCell")
        
        view.backgroundColor = .darkColor

        searchInput.translatesAutoresizingMaskIntoConstraints = false
        categoryStackView.translatesAutoresizingMaskIntoConstraints = false
        forYouLabel.translatesAutoresizingMaskIntoConstraints = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(searchInput)
        
        categoryStackView.addArrangedSubview(cityBikesCategory)
        categoryStackView.addArrangedSubview(mountainBikesCategory)
        
        view.addSubview(categoryStackView)
        view.addSubview(forYouLabel)
               
        view.addSubview(tableView)
        
    }
    
    private func initLayout() {

        let constraints : [NSLayoutConstraint] = [
          
            searchInput.topAnchor.constraint(equalTo: view.topAnchor, constant: .padding4x * 2),
            searchInput.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding4x),
            searchInput.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: (-.padding4x)),
            searchInput.heightAnchor.constraint(equalToConstant: view.frame.height / 17),
            searchInput.widthAnchor.constraint(equalToConstant: view.frame.width - .padding2x),
            
            categoryStackView.topAnchor.constraint(equalTo: searchInput.bottomAnchor, constant: .padding2x),
            categoryStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding2x),
            categoryStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.padding4x),
            
            forYouLabel.topAnchor.constraint(equalTo: categoryStackView.bottomAnchor, constant: .padding4x),
            forYouLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding4x),

            tableView.topAnchor.constraint(equalTo: forYouLabel.bottomAnchor, constant: .padding),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: .padding),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -.padding),

        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    // MARK: - Logic

    private func openDetailScreen(bike: Bike) {
        let detailViewController = DetailViewController(bike: bike)
        self.present(detailViewController, animated: true, completion: nil)
    }

}

 

// MARK: - UITableViewDataSource, UITableViewDelegate

extension StoreViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Bike.availableBikes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "bikeCell", for: indexPath) as? BikeCell else {
            fatalError("Failed to dequeue a cell with identifier: articleCell")
        }
        cell.bike = Bike.availableBikes[indexPath.row]
        //cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .darkColor
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .gray
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let bike = Bike.availableBikes[indexPath.row]
        openDetailScreen(bike: bike)
    }
}
    // MARK: - Text

/*
extension StoreViewController: BikeCellDelegate {
    
    func didTapOpenButton(bike: Bike) {
        openDetailScreen(bike: bike)
    }
}
 */
