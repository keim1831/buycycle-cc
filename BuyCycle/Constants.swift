//
//  Constants.swift
//  BuyCycle
//
//  Created by babycougar on 15/04/2021.
//  Copyright © 2021 individualProject. All rights reserved.
//

import UIKit

extension UIColor{
    static let darkColor = UIColor(red: 5/255, green: 5/255, blue: 5/255, alpha: 1)
    static let sysBlue = UIColor(red: 0/255, green: 170/255, blue: 255/255, alpha: 1)
}

extension UIImage {
    static let bgImage = UIImage(named: "bg")
    static let cityBikes = UIImage(named: "cityBikes")
    static let mountainBikes = UIImage(named: "mountainBikes")
    static let preview = UIImage(named: "hidden")
    static let search = UIImage(named: "search")
    static let path = UIImage(named: "path")
}

extension CGFloat {
    static let imageSize: CGFloat = 500
    static let inputWidth: CGFloat = 500
    static let inpputHeight: CGFloat = 50
    static let padding: CGFloat = 8
    static let padding2x: CGFloat = 16
    static let padding4x: CGFloat = 32
}

